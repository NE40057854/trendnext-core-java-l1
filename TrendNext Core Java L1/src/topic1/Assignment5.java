package topic1;

import java.util.Scanner;

public class Assignment5 {
	public static void main(String[] args) {
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the 4-digit no.");
		int a=sc.nextInt();
		int sum=0;
		while(a>0)
		{
			int r= a%10;
			sum+=r;
			a=a/10;
		}
		System.out.println("Sum is="+sum);
	}

}
