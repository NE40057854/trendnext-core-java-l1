package topic5;
import topic5.testpackage.Foundation;

public class Assignment1 {

	public static void main(String[] args) {
		Foundation obj = new Foundation();
		
		System.out.println("Var1 = "+ obj.Var1); // not accessible
		System.out.println("Var2 = "+ obj.Var2); // not accessible
		System.out.println("Var3 = "+ obj.Var3); // not accessible
		System.out.println("Var4 = "+ obj.Var4); //accessible
		

	}

}
