package topic2;


class Payment
{
	double amount;
	double getamount()
	{
	return amount;	
	}
	void setamount(double a)
	{
		amount=a;
	}
	void paymentDetails()
	{
		System.out.println("Amount of the payment = "+ amount);
	}
}
class CashPayment extends Payment
{
	
	void paymentDetails()
	{
		System.out.println("Payment is in Cash"+ amount);
	}
	
}

class CreditCardPayment extends Payment
{
	String cardname;
	String exp;
	int cardno;
	
	CreditCardPayment(String a,String b,int c)
	{
		cardname=a;
		exp=b;
		cardno=c;
	}
	void paymentDetails()
	{
		super.paymentDetails();
		System.out.println(cardname+ " "+exp+" "+cardno+" "+amount);
	}
	
}

public class Assignment4 {


	public static void main(String[] args) {
		CashPayment obj1 = new CashPayment();
		CashPayment obj2 = new CashPayment();
		CreditCardPayment obj3 = new CreditCardPayment("Neha Singh","23-05-2025",123456);
		CreditCardPayment obj4 = new CreditCardPayment("Jungkook","12-07-2030",789345);
		obj1.setamount(2000);
		System.out.println(obj1.getamount());
		obj1.paymentDetails();
		obj2.paymentDetails();
		obj3.paymentDetails();
		obj4.paymentDetails();
		

	}

	

}
