package topic2;

class Book1 {
 long isbn;
 String title;
 int price;
 Book1(long a, String b, int c)
 {
	isbn=a;
	title=b;
	price=c;
 }
}

class Magazine extends Book1
{
	String type;
	Magazine(long a, String b, int c,String m)
	{ super(a,b,c);
		type=m;
	}
	public String toString()
	{
		return type+" "+isbn+" "+title+" "+price;
	}
	
}

class Novel extends Book1
{
	String author;
	Novel(long a, String b, int c,String n)
	{ super(a,b,c);
		author=n;
	}
	public String toString()
	{
		return author+" "+isbn+" "+title+" "+price;
	}
	

}

class Books
{
	
	public static void main(String[] args) {
		
   Magazine m= new Magazine(123456,"Vogue",2000,"Fashion");
   Novel n= new Novel(23489,"Harry Potter",3000,"J.K. Rowling");
   System.out.println(m);
   System.out.println(n);
   
   
	}

}
