package topic2;


 abstract class Instrument {
	abstract void play();

}
  class Piano extends Instrument
 {
	 void play()
	 {
		 System.out.println("tan tan tan tan");
	 }
 }
 class Flute extends Instrument
 {
	  void play()
		 {
			 System.out.println("tan tan tan tan");
		 }
	 
 }
  class Guitar extends Instrument
 {
	 void play()
		 {
			 System.out.println("tin tin tin");
		 }

 }
public abstract class Assignment5 {

	

	public static void main(String[] args) {
		Instrument[] obj = new Instrument[10];
		Instrument f1=new Flute();
		Instrument p1=new Piano();
		Instrument g1=new Guitar();
		Instrument f2=new Flute();
		Instrument p2=new Piano();
		Instrument g2=new Guitar();
		Instrument f3=new Flute();
		Instrument p3=new Piano();
		Instrument g3=new Guitar();
		obj[0]=f1;
		obj[1]=g1;
		obj[2]=p1;
		obj[3]=f2;
		obj[4]=g2;
		obj[5]=p2;
		obj[7]=f3;
		obj[8]=g3;
		obj[9]=p3;
		f1.play();
		p1.play();
		g1.play();
		
		for(int i=0;i<10;i++)
		{
			if(obj[i] instanceof Instrument)
			{
				System.out.println(obj[i]+ "is stored at index "+ i);
			}
		
		}
		

	}

}
