package topic4;

import java.util.HashSet;
import java.util.Iterator;

public class Assignment5 {

	public static void main(String[] args) {
		HashSet <String> set = new HashSet<String>();
		set.add("Neha");
		set.add("BTS");
		set.add("Kim Taehyung");
		set.add("Kim Namjoon");
		
		Iterator<String> i= set.iterator();
		while(i.hasNext())
		{
			System.out.println(i.next());
		}

	}

}
