package topic4;

import java.util.HashMap;
import java.util.Scanner; 



public class Telephone { 
	
	static HashMap<String, Integer> map = new HashMap<>(); 


	public void findNumber(String name) {
		if (map.containsKey(name)) { 

            Integer a = map.get(name); 

            System.out.println("Phone Number of "  +  name +" is:- " + a); 

        } 
		else
			System.out.println("No Data Found ");
			
	}
	

    public static void main(String[] args) 

    { 
    	
		Scanner sc=new Scanner(System.in);

       map.put("Neha",98625374); 
        map.put("RM", 946584752); 
        
        Telephone obj = new Telephone();
        
        System.out.println("Enter the name");
          String name = sc.nextLine();
		obj .findNumber(name);

    } 
}
