package topic4;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class Employee
{
	String name;
	int ecode;
	String salary;
	
	public Employee(String n, int i, String j) {
		name=n;
		ecode=i;
		salary=j;
	}
	
}
class EmployeeDB extends Employee
{
	static List <Employee> list= new ArrayList<Employee>();
	public EmployeeDB(String n, int i, String j) {
		super(n, i, j);
		
	}

	boolean addEmployee(Employee e)
	{
		list.add(e);
		
		return true;
		
	}
	
	boolean deleteEmployee(int eCode)
	{
		if(this.ecode==eCode)
			{
				list.remove(this);
				return true;
			}
		else
			return false;
		
	}
	
	String showPaySlip(int eCode)
	{
		if(this.ecode==eCode)
		{
			return this.salary;
	 
		}
	else
		return "Invalid eCode";
		
	}
	
	Employee[] listAll()
	{ 
		int i=0;
		Employee[] e = new Employee[5];
		Iterator<Employee> vi=list.iterator();
        while(vi.hasNext())
        {
            e[i]=vi.next();
            i++;
        }
        return e;
		
	}
	
	public static void main(String[] args)
	{
		
		
		EmployeeDB obj1 = new EmployeeDB("Neha",400234, "14500");
		Employee obj2 = new Employee("RM",500234, "15500");
		EmployeeDB obj3 = new EmployeeDB("TaeTae",600234, "16500");
		list.add(obj1);
		list.add(obj3);
		obj1.addEmployee(obj2);
		System.out.println(obj1.deleteEmployee(400234));
		System.out.println("Salary =" +obj3.showPaySlip(600234));
		Employee[] e=obj1.listAll();
		for(int i=0;i<e.length;i++)
		{  if(e[i]!=null) {
			System.out.println("Name = " + e[i].name);
			System.out.println("Salary = " + e[i].salary);
			System.out.println("ECode = " + e[i].ecode);
		}
		}
		
	}
}


