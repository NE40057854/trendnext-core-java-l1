package topic4;

class Number implements Runnable
{
	Thread t;
	double n;
	
	Number(double n2)
	{
		n=n2;
		t=new Thread(this,"NumberThread");
		
		t.start();
	}
	
	public void run()
	{
		try
		{ 
			System.out.println("\n Number : "+n);
		
		}
		catch (Exception e)
		{
			System.out.println(e);
		}
		
	}
}

class Factorial implements Runnable
{
	Thread t;
	double n;
	
	Factorial(double n2)
	{
		n = n2;
		t=new Thread(this,"Factorial Thread");
		t.start();
	}
	
	public void run()
	{
		try
		{
			int f = 1;
			for(int i = 2;i<=n;i++)
			{
				f*=i;
			}
			System.out.print("Factorial of "+n+" is : "+f);
		}
		catch (Exception e)
		{
			System.out.println(e);
		}
		
	}
}

class Assignment1
{
	public static void main(String []args)
	{ 
		for(int i=0;i<5;i++)
		{
			double n =  Math.random();
			Number obj = new Number(n);
			Factorial obj2 = new Factorial(n);
		
			try
			{
				obj2.t.join();
				obj.t.join();
			}
			catch (InterruptedException e)
			{
				System.out.println(e);
			}
		}
		System.out.println("\nExiting main thread");
	}
}
