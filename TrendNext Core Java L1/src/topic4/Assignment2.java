package topic4;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class Assignment2 {
    
    Timer timer;

    public Assignment2() {
	
        timer = new Timer();
        timer.schedule(new RemindTask(),0,2*1000); 
    }

    class RemindTask extends TimerTask {
	int n = 10;

        public void run() {
	    if (n > 0) {
	        Calendar calendar = Calendar.getInstance();
	        Date time = calendar.getTime();
		System.out.println("Time :" + time);
		n--;
	    } else {
	            System.out.println("Time's up!");
               
	        timer.cancel(); 
	        System.exit(0);   
	    }
        }
    }

    public static void main(String args[]) {
	System.out.println("About to schedule task.");
        new Assignment2();
	System.out.println("Task scheduled.");
    }
}




