package topic3;

import java.util.Scanner;

/*Write a program to check whether the given string is a palindrome or not.
[Hint :You have to extract each character from the beginning and end of the String 
and compare it with each other. String x=”Malayalam”; char c= x.charAt(i) where i is the index]*/
public class Assignment4 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter String");
		String b= sc.nextLine();
		String a= b.toLowerCase();
		int f=1;
		for(int i=0;i<a.length()/2;i++)
		{
			char x = a.charAt(i);
			char y = a.charAt(a.length()-1-i);
			if(x==y)
				f=1;
			else
				f=0;
		}
		if(f==1)
			System.out.println("Palindrome");
		else
			System.out.println("Not Palindrome");

	}

}
