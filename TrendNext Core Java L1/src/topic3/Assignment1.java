package topic3;
/* Write a program to accept name and age of a person from the command prompt(passed as arguments when you execute the class) 
 * and ensure that the age entered is >=18 and < 60. Display proper error messages. The program must exit gracefully 
 * after displaying the error message in case the arguments passed are not proper.
 *  (Hint : Create a user defined exception class for handling errors.)*/
class InvalidAgeException extends Exception
{
	InvalidAgeException(String s)
	{
		super(s);
	}
}
public class Assignment1 {
	static void validate (int age) throws InvalidAgeException
	{
		if(age<18 || age>60)
			throw new InvalidAgeException("Not Valid");
		else
			System.out.println("Welcome");
	}
	
	public static void main(String args[])
	{
		try
		{
			int x= Integer.parseInt(args[0]);
			validate(x);
		}
		catch(Exception e)
		{
			System.out.println("Exception Occured:" + e);
		}
		
	}

}
